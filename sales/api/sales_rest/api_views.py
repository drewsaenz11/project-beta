from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import AutomobileVOEncoder, SalesRecordListEncoder, CustomerEncoder, EmployeeEncoder

from .models import (
    AutomobileVO,
    Employee,
    Customer,
    SalesRecord,
)


@require_http_methods(["GET", "POST"])
def api_list_sales(request, employee_id=None):
    if request.method == 'GET':
        if employee_id is None:
            sales = SalesRecord.objects.all()
            return JsonResponse(
                {'sales': sales},
                encoder=SalesRecordListEncoder,
            )
        else:
            employee = Employee.objects.get(number=employee_id)
            sales = SalesRecord.objects.filter(employee=employee)

            return JsonResponse(
                {'sales': sales},
                encoder=SalesRecordListEncoder
            )
    else:
        content = json.loads(request.body)

        try:
            employee_id = content['employee']
            employee = Employee.objects.get(number=employee_id)
            content['employee'] = employee
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Employee Does Not Exist"},
                status=404,
            )

        try:
            customer_id = content['customer']
            customer = Customer.objects.get(id=customer_id)
            content['customer'] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer Does Not Exist"},
                status=404,
            )

        try:
            automobile_href = content['automobile']
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "automobile Does Not Exist"},
                status=404,
            )

        content['sales_price'] = float(content['sales_price'])

        sale = SalesRecord.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SalesRecordListEncoder,
            safe=False,
        )
        

@require_http_methods(['GET', "POST"])
def api_create_customer(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        return JsonResponse(
            {'customers': customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(['GET', "POST"])
def api_create_employee(request):
    if request.method == "GET":
        employees = Employee.objects.all()
        return JsonResponse(
            {'employees': employees},
            encoder=EmployeeEncoder,
        )
    else:
        content = json.loads(request.body)
        employee = Employee.objects.create(**content)
        return JsonResponse(
            employee,
            encoder=EmployeeEncoder,
            safe=False,
        )


@require_http_methods(['GET'])
def api_list_available(request):
    cars = AutomobileVO.objects.all()
    available = []
    for car in cars:
        if not car.sales_record.all():
            available.append(car)
    return JsonResponse(
        {'automobiles': available},
        encoder=AutomobileVOEncoder,
    )
