from common.json import ModelEncoder

from .models import AutomobileVO, Employee, SalesRecord, Customer

class EmployeeEncoder(ModelEncoder):
    model = Employee
    properties = [
        'name',
        'number',
        'id'
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'name',
        'id'
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'import_href',
        'vin',
        'name'
    ]


class SalesRecordListEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        'employee',
        'customer',
        'automobile',
        'sales_price',
        'id',
    ]

    encoders = {
        'employee': EmployeeEncoder(),
        'customer': CustomerEncoder(),
        'automobile': AutomobileVOEncoder(),
    }