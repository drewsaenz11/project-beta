import React, { Component } from 'react'

export default class EmployeeForm extends Component {

  constructor() {
    super();
    this.state = {
        name: '',
        number: '',
        successClass: "alert alert-success d-none",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.timer = this.timer.bind(this);
}

handleChange(event) {
    const value = event.target.value;
    this.setState({ [event.target.name]: value })
}

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.successClass;
    console.log(data);

    const employeeUrl = 'http://localhost:8090/api/employee/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(employeeUrl, fetchConfig);
    if (response.ok) {
      const newEmployee = await response.json();
      console.log(newEmployee);

      const cleared = {
        name: '',
        number: '',
        successClass: "alert alert-success",
      };
      this.setState(cleared);
    }
  }

  timer() {
    setTimeout(() => {
      return this.setState({ successClass: "alert alert-success d-none" });;
    }, 4000);
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>New Employee</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input value={this.state.name} onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="Name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.number} onChange={this.handleChange} placeholder="Employee Number" required type="int" name="number" id="number" className="form-control" />
                <label htmlFor="number">Employee Number</label>
              </div>
              <div className={this.state.successClass} id="success-alert">
                Emplyee Added.
              </div>
              <button onClick={this.timer} className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
