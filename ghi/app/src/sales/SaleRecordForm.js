import React, { Component } from 'react'

export default class SaleRecordForm extends Component {

    constructor() {
        super();
        this.state = {
            employee: '',
            employees: [],
            customer: '',
            customers: [],
            automobile: '',
            automobiles: [],
            sales_price: '',
            successClass: "alert alert-success d-none",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.timer = this.timer.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({ [event.target.name]: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        let data = { ...this.state };
        const sales_price = parseFloat(data['sales_price'])
        data['sales_price'] = sales_price;
        delete data.employees;
        delete data.customers;
        delete data.automobiles;
        delete data.successClass;
        console.log(data);

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            console.log(newSale);

            const cleared = {
                employee: '',
                customer: '',
                automobile: '',
                sales_price: '',
                successClass: "alert alert-success",
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const employeeUrl = 'http://localhost:8090/api/employee/';
        const customerUrl = 'http://localhost:8090/api/customer/';
        const automobileUrl = 'http://localhost:8090/api/automobiles/available/';

        const employeeResponse = await fetch(employeeUrl);
        const customerResponse = await fetch(customerUrl);
        const automobileResponse = await fetch(automobileUrl);

        if (employeeResponse.ok && customerResponse.ok && automobileResponse.ok) {
            const employeeData = await employeeResponse.json();
            const customerData = await customerResponse.json();
            const automobileData = await automobileResponse.json();

            this.setState({
                employees: employeeData.employees,
                customers: customerData.customers,
                automobiles: automobileData.automobiles,
            })
        }
    }

    timer() {
        setTimeout(() => {
            return this.setState({ successClass: "alert alert-success d-none" });;
        }, 4000);
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Sale Record</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="mb-3">
                                <select value={this.state.employee} onChange={this.handleChange} required name="employee" id="employee" className="form-select">
                                    <option value="">Employee</option>
                                    {this.state.employees?.map(employee => {
                                        return (
                                            <option key={employee.number} value={employee.number}>
                                                {employee.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.customer} onChange={this.handleChange} required name="customer" id="customer" className="form-select">
                                    <option value="">Customer</option>
                                    {this.state.customers?.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>
                                                {customer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.automobile} onChange={this.handleChange} required name="automobile" id="automobile" className="form-select">
                                    <option value="">Automobile</option>
                                    {this.state.automobiles?.map(automobile => {
                                        return (
                                            <option key={automobile.import_href} value={automobile.import_href}>
                                                {automobile.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.sales_price} onChange={this.handleChange} placeholder="Sales Price" required type="number" name="sales_price" id="sales_price" className="form-control" />
                                <label htmlFor="sales_price">Sales Price</label>
                            </div>
                            <div className={this.state.successClass} id="success-alert">
                                Sales Record Added.
                            </div>
                            <button onClick={this.timer} className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
