import React from 'react';
import { useState, useEffect } from 'react';

export default function AutomobilesList() {

  const [automobiles, setAutomobiles] = useState([]);

  const fetchAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const result = await fetch(url);
    const recordsJSON = await result.json();
    setAutomobiles(recordsJSON.autos);
  }

  useEffect(() => {
    fetchAutomobiles()
  }, []);

  return (
    <div className="row">
      <div className='className="offset-3 col-6"'>
        <div className="form-floating col-sm-5">
          <h1 className='mt-4'>Automobiles</h1>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {automobiles?.map(automobile => {
            return (
              <tr key={automobile.id}>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>

              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}
