import React, { Component } from 'react'

export default class ManufacturerForm extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      successClass: "alert alert-success d-none",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.timer = this.timer.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;
    this.setState({ [event.target.name]: value })
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.successClass;
    console.log(data);

    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      const newManufacturer = await response.json();
      console.log(newManufacturer);

      const cleared = {
        name: '',
        successClass: "alert alert-success",
      };
      this.setState(cleared);
    }
  }

  timer() {
    setTimeout(() => {
      return this.setState({ successClass: "alert alert-success d-none" });;
    }, 4000);
  }
  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>New Manufacturer</h1>
            <form onSubmit={this.handleSubmit}>
              <div className="form-floating mb-3">
                <input value={this.state.name} onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="Name">Name</label>
              </div>
              <div className={this.state.successClass} id="success-alert">
                Manufacturer Added.
              </div>
              <button onClick={this.timer} className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
