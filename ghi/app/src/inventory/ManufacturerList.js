import React from 'react';
import { useState, useEffect } from 'react';

export default function ManufacturerList() {

  const [manufacturers, setManufacturers] = useState([]);

  const fetchManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const result = await fetch(url);
    const recordsJSON = await result.json();
    setManufacturers(recordsJSON.manufacturers);
  }

  useEffect(() => {
    fetchManufacturers()
  }, []);

  return (
    <div className="row">
      <div className='className="offset-3 col-6"'>
        <div className="form-floating col-sm-5">
        <h1 className='mt-4'>Manufacturers</h1>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers?.map(manufacturer => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}
