import React from 'react';
import { useState, useEffect } from 'react';

export default function ModelList() {

  const [models, setModels] = useState([]);

  const fetchModels = async () => {
    const url = 'http://localhost:8100/api/models/';
    const result = await fetch(url);
    const recordsJSON = await result.json();
    setModels(recordsJSON.models);
  }

  useEffect(() => {
    fetchModels()
  }, []);

  return (
    <div className="row">
      <div className='className="offset-3 col-6"'>
        <div className="form-floating col-sm-5">
          <h1 className='mt-4'>Vehicle Models</h1>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models?.map(model => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td className="photo">
                  <img className="photo" src={model.picture_url} />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}
