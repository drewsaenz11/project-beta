import React, { Component } from 'react'

export default class AutomobilesForm extends Component {
  constructor() {
    super();
    this.state = {
      color: '',
      year: '',
      vin: '',
      model_id: '',
      models: [],
      successClass: "alert alert-success d-none",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.timer = this.timer.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;
    this.setState({ [event.target.name]: value })
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.successClass;
    delete data.models;
    console.log(data);

    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      const newAutomobile = await response.json();
      console.log(newAutomobile);

      const cleared = {
        color: '',
        year: '',
        vin: '',
        model_id: '',
        successClass: "alert alert-success",
      };
      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }

  timer() {
    setTimeout(() => {
      return this.setState({ successClass: "alert alert-success d-none" });;
    }, 4000);
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>New Automobile</h1>
            <form onSubmit={this.handleSubmit}>
              <div className="form-floating mb-3">
                <input value={this.state.color} onChange={this.handleChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="Name">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.year} onChange={this.handleChange} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                <label htmlFor="Year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.vin} onChange={this.handleChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="VIN">VIN</label>
              </div>
              <div className="mb-3">
                <select value={this.state.model_id} onChange={this.handleChange} placeholder="Vehicle Model" required type="" name="model_id" id="model_id" className="form-control">
                  <option value="">Model</option>
                  {this.state.models?.map(model => {
                    return (
                      <option key={model.id} value={model.id}>
                        {model.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className={this.state.successClass} id="success-alert">
                Model Added.
              </div>
              <button onClick={this.timer} className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
