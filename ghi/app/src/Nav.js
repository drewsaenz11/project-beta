import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="main_nav">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown">
                <a className="nav-link" type="button" id="navbarDarkDropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                  Inventory
                </a>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><NavLink className="dropdown-item" aria-current="page" to="inventory/manufacturer/list/">Manufacturers</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="inventory/manufacturer/new/">New Manufacturer</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="inventory/models/list/">Vehicle Models</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="inventory/models/new/">New Vehicle Model</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="inventory/automobiles/list/">Automobiles</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="inventory/automobiles/new/">New Automobile</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link" type="button" id="navbarDarkDropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                  Sales
                </a>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><NavLink className="dropdown-item" aria-current="page" to="sales/list/">Sales Records</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="sales/new/">New Sales Record</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="sales/customer/">New Customer</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="sales/employee/">New Employee</NavLink></li>
                </ul>
            </li>
            <li className="nav-item dropdown">
                <a className="nav-link" type="button" id="navbarDarkDropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                  Service
                </a>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><NavLink className="dropdown-item" aria-current="page" to="service/list/">Service Appointments</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="service/history/">Service History</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="service/new/">New Appointment</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="service/technician/">New Technician</NavLink></li>
                </ul>
            </li>
          </ul>
        </div>
        
      </div>
    </nav>
  )
}

export default Nav;
